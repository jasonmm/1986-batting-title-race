(defproject batting-title "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/java.jdbc "0.7.5"]
                 [yesql "0.5.3"]
                 [mysql/mysql-connector-java "5.1.6"]
                 [environ "1.1.0"]]
  :main ^:skip-aot batting-title.core
  :target-path "target/%s"
  :plugins [[lein-gorilla "0.4.0"]]
  :profiles {:uberjar {:aot :all}})
