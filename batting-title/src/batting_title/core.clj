(ns batting-title.core
  (:require [yesql.core :as sql]
            [environ.core :refer [env]]))

;;; Database connection.
(def db {:classname   "com.mysql.jdbc.Driver"
         :subprotocol "mysql"
         :subname     (str "//" (env :dbhostname) ":3306/" (env :dbdatabase))
         :user        (env :dbusername)
         :password    (env :dbpassword)})

;;; Characters that deonote a hit occurred.
(def hit-chars [\S \D \T \H])

;;; Characters that denote a safety occurred.
(def safety-chars [\S \D \T \H \W])

;;; Keys in event maps that are statistics.
(def stat-keys [:atbats :plateappearances :hits :outs :singles :doubles :triples :homeruns :walks :strikeouts])

(sql/defqueries "queries.sql" {:connection db})

(defn tf->bool
  "If the given string is a 'T' then return true, else return false."
  [s]
  (if (= s "T") true false))

(defn bool->int
  "If `b` is truthy then return 1, otherwise return 0."
  [b]
  (if b 1 0))

(defn booleanize
  "Takes a database record and converts known boolean event fields from 'T' or
  'F' to `true` or `false`."
  [event]
  (reduce (fn [carry x] (update-in carry [x] tf->bool))
          event
          [:bat_event_fl :ab_fl]))

(defn set-outcome
  "Takes an event map created from Retrosheet data and adds new keywords to be
  used for calculating statistics.

  The returned `event` map has added keys indicating whether or not the event
  describes that outcome. Later these will be summed creating a count of the
  number of times that outcome occurred. That is why the keys are named as
  they are."
  [event]
  (let [first-char (first (:event_tx event))]
    (assoc event
      :first-char first-char                                ;; only useful for debugging.
      :game_id (subs (:game_id event) 3)
      :atbats (bool->int (:ab_fl event))
      :plateappearances (bool->int (:bat_event_fl event))
      :hits (bool->int (.contains hit-chars first-char))
      :outs (bool->int (not (.contains safety-chars first-char)))
      :singles (bool->int (= \S first-char))
      :doubles (bool->int (= \D first-char))
      :triples (bool->int (= \T first-char))
      :homeruns (bool->int (= \H first-char))
      :walks (bool->int (= \W first-char))
      :strikeouts (bool->int (= \K first-char)))))


(defn add-event-to-total
  "Add the value of certain keys in `event` to the `total` map. Returns an
  updated version of `total`."
  [total event]
  (reduce (fn [carry key]
            (update carry key + (key event)))
          total
          stat-keys))

(defn sum-events
  "Takes a seq of event maps and returns an event map containing the sum of
  certain keys."
  [events]
  (reduce (fn [carry event]
            (if (empty? carry) event (add-event-to-total carry event)))
          {}
          events))

(defn game-totals
  "Calculates the total stats for a game from a seq of game events."
  [[gameid events]]
  [gameid (sum-events events)])

(defn create-window-data
  "Creates a map of only the necessary data needed in a window from a vector of
  raw window data."
  [raw-window]
  (let [game-numbers     (map first raw-window)
        total-game-stats (sum-events (map second raw-window))]
    {:min-game (apply min game-numbers)
     :max-game (apply max game-numbers)
     :stats    (select-keys total-game-stats stat-keys)}))

(defn ws
  "Helper function for getting a stat from a window."
  [window stat]
  (get-in window [:stats stat]))

(defn window->bavg
  "Take window data and convert it to data for a plot of batting average."
  [window]
  (let [batting-avg (/ (ws window :hits) (ws window :atbats))]
    [(:min-game window) batting-avg]))

(defn window->oba
  "Take window data and convert it ot data for a plot of on base average"
  [window]
  (let [on-base-avg (/ (+ (ws window :walks) (ws window :hits)) (ws window :plateappearances))]
    [(:min-game window) on-base-avg]))

(defn window->slg
  "Take window data and convert it to data for a plot of slugging average"
  [window]
  (let [double-bases  (* (ws window :doubles) 2)
        triple-bases  (* (ws window :triples) 3)
        homerun-bases (* (ws window :homeruns) 4)
        total-bases   (+ (ws window :singles) double-bases triple-bases homerun-bases)
        slugging-avg  (/ total-bases (ws window :atbats))]
    [(:min-game window) slugging-avg]))

(defn create-stats-windows
  "The 'main' function that drives creating the stat windows."
  [player-id year window-size]
  (->> {:player_id player-id :year year}
       player-events
       (mapv (comp booleanize set-outcome))
       (group-by :game_id)
       (into (sorted-map))
       (map game-totals)
       (map-indexed (fn [idx [_ events]] [idx events]))
       (partition window-size 1)
       (map create-window-data)))





