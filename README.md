# Description

Contains an exploration of the 1986 MLB batting title race using the [Gorilla REPL](http://gorilla-repl.org/).

# Usage

Clone the repository and then run:

```bash
$ cd 1986-batting-title-race/batting-title
$ lein gorilla :port 8999
```

Finally, open http://127.0.0.1:8999/worksheet.html?filename=worksheets/1986-batting-title-race.clj in a browser.

# Jupyter Notebook

Previously, this analysis was done in a Python Jupyter Notebook, that
notebook is included in this repository as well.

